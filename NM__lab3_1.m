f=@(x) cos(77*asin(0.3*sqrt((x+4)/7.7)));

left_point = -2; 
right_point = 2;

for N=[15, 30, 60]
    step = (right_point-left_point)/N; 
    
    X = left_point:step:right_point;
    i_num = 0;
    
    for i=1:N
        i_num = i_num + (X(i+1)-X(i))*f((X(i+1)+X(i))/2);
    end
    
    i_exact = integral (f,left_point,right_point);
    
    disp(N);
    disp('i_num i_exact i_num-i_exact');
    disp([i_num,i_exact,i_num-i_exact]);
end
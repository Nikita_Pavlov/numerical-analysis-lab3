f=@(x) cos(77*asin(0.3*sqrt((x+4)/7.7)));
df = @(x) -4.16233*sin(77*asin(0.108112*sqrt(x+4)))...
    /(sqrt(x+4)*sqrt(0.953247-0.0116883));

left_point = -2; 
right_point = 2;
N=1
step = (right_point-left_point)/(N-1); 

x = left_point:step:right_point;
y = f(x);

dfc=zeros(N-2);
dfr=zeros(N-1);

dfr(1)=(f(x(2))-f(x(1)))/step;
for i=2:N-1
    dfc(i-1)=(y(i+1)-y(i-1))/(2*step);
    dfr(i)=(y(i+1)-y(i))/(step);
end

figure(1);
fplot(df,[left_point right_point]);
hold on;

for i=1:N-2
   scatter(x(i+1),dfc(i),20,'o','filled',"black");
end

grid;
hold off;

figure(2);
fplot(df,[left_point right_point]);
hold on;

for i=1:N-2
   scatter(x(i),dfr(i),20,'o','filled',"black");
end

grid
hold off
